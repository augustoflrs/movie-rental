package com.eafg.movie.rental.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private BigDecimal amount;

    private Date paymentDate;
   
}
