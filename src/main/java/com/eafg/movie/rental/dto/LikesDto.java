package com.eafg.movie.rental.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LikesDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private MovieDto movieId;

    private UserDto userId;

}
