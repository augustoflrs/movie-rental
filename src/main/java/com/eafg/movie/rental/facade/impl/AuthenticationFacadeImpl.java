package com.eafg.movie.rental.facade.impl;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.eafg.movie.rental.facade.AuthenticationFacade;
@Component
public class AuthenticationFacadeImpl implements AuthenticationFacade  {

	@Override
	public Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

}
