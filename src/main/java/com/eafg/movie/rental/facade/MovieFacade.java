package com.eafg.movie.rental.facade;

import java.util.List;

import com.eafg.movie.rental.dto.InventoryDto;
import com.eafg.movie.rental.dto.MovieDto;

public interface MovieFacade {
	List<MovieDto> getList(int page, int size, String sortDir, String sort);

	void update(MovieDto value);

	MovieDto create(MovieDto value);

	MovieDto getById(Integer id);
	
	void likeMovie(Integer id);
	
	void addMovie(Integer movieId,InventoryDto item);
}
