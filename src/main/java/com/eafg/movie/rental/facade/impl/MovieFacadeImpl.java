package com.eafg.movie.rental.facade.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.eafg.movie.rental.dto.InventoryDto;
import com.eafg.movie.rental.dto.MovieDto;
import com.eafg.movie.rental.facade.AuthenticationFacade;
import com.eafg.movie.rental.facade.MovieFacade;
import com.eafg.movie.rental.mapper.InventoryMapper;
import com.eafg.movie.rental.mapper.MovieMapper;
import com.eafg.movie.rental.model.Inventory;
import com.eafg.movie.rental.model.Likes;
import com.eafg.movie.rental.model.Movie;
import com.eafg.movie.rental.model.Users;
import com.eafg.movie.rental.service.InventoryService;
import com.eafg.movie.rental.service.LikesService;
import com.eafg.movie.rental.service.MovieService;
import com.eafg.movie.rental.service.UserService;

@Service
public class MovieFacadeImpl implements MovieFacade {

	@Autowired
	private MovieService service;
	@Autowired
	private LikesService like;
	@Autowired
	private UserService user;
	@Autowired
	private  InventoryService inventory;
	@Autowired
	private AuthenticationFacade auth;

	@Override
	public List<MovieDto> getList(int page, int size, String sortDir, String sort) {
		List<Movie> movies = service.getMoviesList(page, size, sortDir, sort);
		return movies.stream().map(item -> MovieMapper.MAPPER.modelToDto(item)).collect(Collectors.toList());
	}

	@Override
	public void update(MovieDto value) {
		service.updateMovie(MovieMapper.MAPPER.dtoToModel(value));
	}

	@Override
	public MovieDto create(MovieDto value) {
		return MovieMapper.MAPPER.modelToDto(service.createMovie(MovieMapper.MAPPER.dtoToModel(value)));
	}

	@Override
	public MovieDto getById(Integer id) {
		Movie value = service.getMovieById(id);
		if (value == null) {
			throw new ResourceNotFoundException(String.format("Movie not Found id %s", id));
		}
		return MovieMapper.MAPPER.modelToDto(value);
	}

	@Override
	public void likeMovie(Integer id) {
		Movie value = service.getMovieById(id);
		if (value == null) {
			throw new ResourceNotFoundException(String.format("Movie not Found id %s", id));
		} else {
			Users authUser = user.findOne(auth.getAuthentication().getName());
			Likes newLike = new Likes();
			newLike.setMovieId(value);
			newLike.setUserId(authUser);
			like.create(newLike);
		}
	}

	@Override
	public void addMovie(Integer movieId ,InventoryDto item) {
		Movie value = service.getMovieById(movieId);
		if (value == null) {
			throw new ResourceNotFoundException(String.format("Movie not Found id %s", movieId));
		} else {
			Inventory inventoryItem ;
			inventoryItem = InventoryMapper.MAPPER.dtoToModel(item);
			inventoryItem.setMovieId(value);
			inventory.addInventoryy(inventoryItem);
		}
	}

}
