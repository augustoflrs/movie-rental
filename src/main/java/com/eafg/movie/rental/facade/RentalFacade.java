package com.eafg.movie.rental.facade;

import com.eafg.movie.rental.dto.RentalDto;



public interface RentalFacade {

	void update(RentalDto value);

	RentalDto create(RentalDto value);

	RentalDto getById(Integer id);
	
}
