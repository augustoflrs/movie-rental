package com.eafg.movie.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eafg.movie.rental.model.Inventory;
import com.eafg.movie.rental.repository.InventoryRepository;
import com.eafg.movie.rental.service.InventoryService;

@Service
public class InventoryServiceImpl implements InventoryService {
	@Autowired
	private InventoryRepository repo;

	@Override
	public void addInventoryy(Inventory inventoryItem) {
		repo.save(inventoryItem);
	}

}
