package com.eafg.movie.rental.service;

import com.eafg.movie.rental.model.Likes;

public interface LikesService {
	void create(Likes value);
}
