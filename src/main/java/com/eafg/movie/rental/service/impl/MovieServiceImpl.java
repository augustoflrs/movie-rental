package com.eafg.movie.rental.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.eafg.movie.rental.model.Movie;
import com.eafg.movie.rental.repository.MoviesRepository;
import com.eafg.movie.rental.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService {
	@Autowired
	private MoviesRepository repo;

	@Override
	public List<Movie> getMoviesList(int page, int size, String sortDir, String sort) {
		PageRequest pageReq = PageRequest.of(page, size, Sort.Direction.fromString(sortDir), sort);
		Page<Movie> result = repo.findAll(pageReq);
		return result.getContent();
	}

	@Override
	public void updateMovie(Movie value) {
		repo.save(value);
	}

	@Override
	public Movie createMovie(Movie value) {
		return repo.save(value);
	}

	@Override
	public Movie getMovieById(Integer id) {
		Optional<Movie> result ;
		result =repo.findById(id);
		return result.isPresent()?result.get():null;
	}

}
