package com.eafg.movie.rental.service;

import java.util.List;

import com.eafg.movie.rental.dto.UserDto;
import com.eafg.movie.rental.model.Users;

public interface UserService {

    Users save(UserDto user);
    List<Users> findAll();
    void delete(long id);
    Users findOne(String username);

    Users findById(Long id);
}
