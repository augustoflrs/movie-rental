package com.eafg.movie.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eafg.movie.rental.model.Likes;
import com.eafg.movie.rental.repository.LikesRepository;
import com.eafg.movie.rental.service.LikesService;
@Service
public class LikesServiceImple implements LikesService {
	@Autowired
	private LikesRepository repo;
	@Override
	public void create(Likes value) {
		repo.save(value);
	}

}
