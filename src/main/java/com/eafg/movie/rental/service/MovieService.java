package com.eafg.movie.rental.service;

import java.util.List;

import com.eafg.movie.rental.model.Movie;

public interface MovieService {

	List<Movie> getMoviesList(int page, int size, String sortDir, String sort);

	void updateMovie(Movie value);

	Movie createMovie(Movie value);

	Movie getMovieById(Integer id);
}
