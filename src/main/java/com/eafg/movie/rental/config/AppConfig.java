package com.eafg.movie.rental.config;

import java.text.SimpleDateFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class AppConfig {

	@Bean
	public ObjectMapper objectMapper() {
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(df);
		return mapper;
	}
}
