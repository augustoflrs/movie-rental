package com.eafg.movie.rental.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.eafg.movie.rental.dto.InventoryDto;
import com.eafg.movie.rental.model.Inventory;
@Mapper
public interface InventoryMapper {
	InventoryMapper MAPPER = Mappers.getMapper(InventoryMapper.class);

	Inventory dtoToModel(InventoryDto dto);
	
	InventoryDto modelToDto(Inventory entity);
}
