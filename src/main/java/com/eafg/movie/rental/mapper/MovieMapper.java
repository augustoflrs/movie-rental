package com.eafg.movie.rental.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.eafg.movie.rental.dto.MovieDto;
import com.eafg.movie.rental.model.Movie;

@Mapper
public interface MovieMapper {

	MovieMapper MAPPER = Mappers.getMapper(MovieMapper.class);

	Movie dtoToModel(MovieDto dto);
	
	MovieDto modelToDto(Movie entity);
	
}