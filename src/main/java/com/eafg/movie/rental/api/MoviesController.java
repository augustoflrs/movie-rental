package com.eafg.movie.rental.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.eafg.movie.rental.dto.InventoryDto;
import com.eafg.movie.rental.dto.MovieDto;
import com.eafg.movie.rental.facade.MovieFacade;

@RepositoryRestController
@RequestMapping("/movies")
public class MoviesController extends GenericController{
	@Autowired
	private MovieFacade facade;
	
	@GetMapping(value = "/{page}/{size}/{sort}/{sortDir}")
    @ResponseBody
    public List<MovieDto> getMovies(
            @PathVariable("page") int page,
            @PathVariable("size") int size, 
            @PathVariable("sort") String sort,
            @PathVariable("sortDir") String sortDir) {
	        return facade.getList(page, size, sortDir, sort);
    }
 
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public MovieDto createMovie(@RequestBody MovieDto item) throws ParseException {
        return facade.create(item);
    }
 
    @GetMapping(value = "/{id}")
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public MovieDto getMovie(@PathVariable("id") Integer id) {
        return facade.getById(id);
    }
 
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ADMIN')")
    public void updateMovie(@RequestBody MovieDto item) throws ParseException {
        facade.update(item);
    }

    @PostMapping(value = "/{id}/likes")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public void likes(@PathVariable("id") Integer id) throws ParseException {
         facade.likeMovie(id);
    }
    
    @PostMapping(value = "/{id}/inventory")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @PreAuthorize("hasRole('ADMIN')")
    public void inventory(@PathVariable("id") Integer id, @RequestBody InventoryDto	 item) throws ParseException {
         facade.addMovie(id, item);
    }
    
}
