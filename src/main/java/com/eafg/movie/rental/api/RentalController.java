package com.eafg.movie.rental.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.eafg.movie.rental.dto.RentalDto;
import com.eafg.movie.rental.facade.RentalFacade;
@RepositoryRestController
@RequestMapping("/rentals")
public class RentalController {
	@Autowired
	private RentalFacade facade;
	
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public RentalDto create(@RequestBody RentalDto item) throws ParseException {
        return facade.create(item);
    }
 
    @GetMapping(value = "/{id}")
    @ResponseBody
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public RentalDto get(@PathVariable("id") Integer id) {
        return facade.getById(id);
    }
 
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public void update(@RequestBody RentalDto item) throws ParseException {
        facade.update(item);
    }

}
