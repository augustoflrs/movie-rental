package com.eafg.movie.rental.api;

import java.util.Collections;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public class GenericController {

	protected <T> ResponseEntity<T> handlerException( Exception e) {
		if (e instanceof ResourceNotFoundException) {
			return this.createNotFoundResponse( e.getMessage());
		} else {
			return this.exceptionRespose(e.getMessage());
		}
	}

	private <T> ResponseEntity<T> createNotFoundResponse(String serviceDescription) {
		HttpHeaders httpHeaders = createHeadersResponse();
		return ResponseEntity.notFound().headers(httpHeaders).build();
	}

	private <T> ResponseEntity<T> exceptionRespose( String serviceDescription) {
		HttpHeaders httpHeaders = createHeadersResponse();
		return ResponseEntity.status(500).headers(httpHeaders).build();
	}

	private HttpHeaders createHeadersResponse() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(new MediaType("application", "json")));
		return headers;
	}
}
