package com.eafg.movie.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eafg.movie.rental.model.Inventory;

public interface InventoryRepository extends JpaRepository<Inventory, Integer> {

}
