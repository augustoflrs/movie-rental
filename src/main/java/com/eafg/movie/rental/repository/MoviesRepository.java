package com.eafg.movie.rental.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.eafg.movie.rental.model.Movie;
public interface MoviesRepository extends JpaRepository<Movie, Integer>, PagingAndSortingRepository<Movie, Integer> {
	public Page<Movie> findByTitle(@Param("title") String title, Pageable p);
	public Page<Movie> findByAvailability(@Param("availability") Boolean availability, Pageable p);
}
