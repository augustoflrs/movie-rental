package com.eafg.movie.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eafg.movie.rental.model.Likes;

public interface LikesRepository extends JpaRepository<Likes, Integer>{

}
