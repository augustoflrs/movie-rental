package com.eafg.movie.rental;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Stanley Rodríguez
 * @see    https://www.linkedin.com/in/stanleyrodriguez/
 */
@SpringBootApplication
@Slf4j
public class Application implements CommandLineRunner {

	@Autowired
	private Environment env;

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getDefault());
	}	

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String application = env.getProperty("spring.application.name");
		String environment = env.getProperty("spring.profiles.active");
		String version = env.getProperty("info.version");
		String app = String.format("%s-%s in %s", application, version, environment);
		log.info("Started Application...  {}", app);
	}	
}